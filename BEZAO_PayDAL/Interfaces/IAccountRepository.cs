﻿using BEZAO_PayDAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL.Interfaces
{
    public interface IAccountRepository: IRepository<Account>
    {

    }
}
